/**
 * Created by jiachenpan on 16/11/18.
 */
/* 小写字母*/
export function require(str) {
  return str && str !== ''
}

export function min(str, length) {
  return require(str) && str.length >= length
}

export function max(str, length) {
  return require(str) && str.length <= length
}

export function validPhone(str) {
  const reg = /^(8|7|\+7)\d{10}$/
  return reg.test(str)
}
