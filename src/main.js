import Vue from 'vue'

import '@/styles/index.scss' // global css

import App from './App'
import router from './router'
import store from './store'
import Materialize from 'materialize-css'

window.Materialize = Materialize
Materialize.AutoInit()

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
