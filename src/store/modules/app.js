import axios from 'axios'
var MockAdapter = require('axios-mock-adapter')
var mock = new MockAdapter(axios)

const app = {
  state: {
    form: {
      name: '',
      phone: '',
      message: ''
    }
  },
  mutations: {
    SEND_MESSAGE: (state, form) => {
      state.form.name = form.name
      state.form.phone = form.phone
      state.form.message = form.message
    }
  },
  actions: {
    sendMessage({ commit }, form) {
      console.log(form)
      const name = form.name.trim()
      const phone = form.phone.trim()
      const data = {
        name, phone, message: form.message
      }
      mock.onPost('/contact').reply(200, {
        success: true
      })
      axios.post('/contact', data).then(response => {
        console.log(form)
        commit('SEND_MESSAGE', form)
        var json = JSON.stringify(response.data)
        let blob = new Blob([json], { type: 'text/json' })
        let link = document.createElement('a')
        link.href = window.URL.createObjectURL(blob)
        link.download = 'response.json'
        link.click()  

        console.log(response.data)
        
      })
    }
  }
}

export default app
